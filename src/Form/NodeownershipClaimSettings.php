<?php

namespace Drupal\nodeownership\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Form.
 */
class NodeownershipClaimSettings extends ConfigFormBase {

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nodeownership.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodeownership_claim_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('nodeownership.settings');

    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Retrive configurations.
    // Node types.
    $types = node_type_get_names();
    $options = [];
    foreach ($types as $node_type_id => $node_type_label) {
      $options[$node_type_id] = $node_type_label;
    }

    $form['nodeownership_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types which can be claimed for ownership'),
      '#options' => $options,
      '#default_value' => $config->get('nodeownership_node_types') ?: [],
    ];

    $form['nodeownership_link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Claim Text'),
      '#description' => $this->t('The text that will be shown for the claiming the node'),
      '#default_value' => $config->get('nodeownership_link_text'),
    ];

    $form['nodeownership_pending_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Claim Pending Text'),
      '#description' => $this->t('The text that will be shown for pending claims'),
      '#default_value' => $config->get('nodeownership_pending_text'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory()->getEditable('nodeownership.settings')
        ->set('nodeownership_node_types', $form_state->getValue('nodeownership_node_types'))
        ->save();
    $this->configFactory()->getEditable('nodeownership.settings')
        ->set('nodeownership_link_text', $form_state->getValue('nodeownership_link_text'))
        ->save();
    $this->configFactory()->getEditable('nodeownership.settings')
        ->set('nodeownership_pending_text', $form_state->getValue('nodeownership_pending_text'))
        ->save();

    return parent::submitForm($form, $form_state);
  }

}
